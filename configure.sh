#!/bin/sh

echo "Running Ubuntu configuration script"
echo "Configuring git submodules"
git submodule update --init --recursive
git config status.submodulesummary 1

source $(pwd)/tools/scripts/utils/logger.sh

log_info "Configuring git hooks path"
git config core.hooksPath ./tools/scripts/git/hooks

log_info "Running Ubuntu configuration script -- done"
