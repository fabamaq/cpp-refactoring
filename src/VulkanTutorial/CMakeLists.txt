# Copyright (C) ei06125. All Rights Reserved.

project(VulkanTutorial)

find_package(glfw3 CONFIG REQUIRED)
find_package(glm CONFIG REQUIRED)
find_path(STB_INCLUDE_DIRS "stb.h")
find_package(tinyobjloader CONFIG REQUIRED)
include(FindVulkan)

if(NOT Vulkan_FOUND)
  log_fatal("Vulkan NOT FOUND! Vulkan Tutorial requires a Vulkan SDK (duh)")
endif()

add_executable(VulkanTutorial)
file(GLOB_RECURSE VulkanTutorial_SOURCES "*.cpp")
target_sources(VulkanTutorial PRIVATE ${VulkanTutorial_SOURCES})

# Configure PWD into the executable with generated VulkanTutorial header file
set(PWD ${CMAKE_CURRENT_LIST_DIR})
configure_file(
  "sources/VulkanTutorial.hpp.in" "VulkanTutorial.hpp" ESCAPE_QUOTES
)

target_include_directories(
  VulkanTutorial PRIVATE ${STB_INCLUDE_DIRS} ${Vulkan_INCLUDE_DIR}
                         ${PROJECT_BINARY_DIR}
)

target_link_libraries(
  VulkanTutorial PRIVATE glfw glm::glm tinyobjloader::tinyobjloader
                         ${Vulkan_LIBRARY}
)
