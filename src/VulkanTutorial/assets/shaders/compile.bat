@echo off
echo "[compile.bat][TRACE] Compiling Vulkan Tutorial shaders"
echo %VULKAN_SDK%
%VULKAN_SDK%/Bin/glslc.exe shader.vert -o vert.spv
%VULKAN_SDK%/Bin/glslc.exe shader.frag -o frag.spv
echo "[compile.bat][TRACE] Compiling Vulkan Tutorial shaders -- done"
pause
