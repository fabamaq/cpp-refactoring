#!/bin/sh

echo ${VULKAN_SDK}
echo "[compile.sh][TRACE] Compiling Vulkan Tutorial shaders with $(basename ${VULKAN_SDK})"
cd "$(dirname $0)"
${VULKAN_SDK}/Bin/glslc.exe shader.vert -o vert.spv
${VULKAN_SDK}/Bin/glslc.exe shader.frag -o frag.spv
cd -
# ${VULKAN_SDK}/x86_64/bin/glslc shader.vert -o vert.spv
# ${VULKAN_SDK}/x86_64/bin/glslc shader.frag -o frag.spv

echo "[compile.sh][TRACE] Compiling Vulkan Tutorial shaders -- done"
